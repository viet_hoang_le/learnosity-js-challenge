declare var TILE_WIDTH: number;
declare var TILE_HEIGHT: number;

enum Status {
  IDLE = 'IDLE',
  PROCESSING = 'PROCESSING',
  DONE = 'DONE'
}

enum WorkerCommand {
  CALCULATE_AVERAGE_COLOR = 'CALCULATE_AVERAGE_COLOR',
  FETCH_COLOR = 'FETCH_COLOR',
  STOP = 'STOP',
}

interface CacheColorPng {
  [hexColor: string]: {
    tile: HTMLImageElement;
    isReady: boolean;
  };
}

interface TileGrid {
  column: number;
  hexColor: string;
  tile?: HTMLImageElement;
}

interface RGB {
  r: number; // red channel
  g: number; // green channel
  b: number; // blue channel
}

interface CalculateAverageColorReq {
  row: number;
  col: number;
  imageData: ImageData['data'];
}
interface CalculateAverageColorRes {
  row: number;
  col: number;
  hexColor: string;
}
interface FetchColorReq {
  row: number;
  col: number;
  tileSize: number;
  hexColor: string;
}
interface FetchColorRes {
  row: number;
  col: number;
  hexColor: string;
  blob: Blob;
}
interface WebWorkerData {
  cmd: WorkerCommand,
  args:
    CalculateAverageColorReq |
    CalculateAverageColorRes |
    FetchColorReq |
    FetchColorRes;
}

interface CanvasElements {
  canvasImage: HTMLCanvasElement,
  canvasMosaic: HTMLCanvasElement,
  canvasMosaicCustom: HTMLCanvasElement,
}