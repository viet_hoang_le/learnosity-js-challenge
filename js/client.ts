// Checking whether the Browser supports Worker or not
if (!this.window.Worker) {
  console.error('This Browser does not support Worker');
  const alertMessage = 'This app is leverage Worker for optimization. ' +
    'This browser does not support Web Worker, however, ' +
    'still you can extract Code & Logic to test by yourself.';
  alert(alertMessage);
}

// When the document is ready, initializing...
document.addEventListener('DOMContentLoaded', function(){
  const canvasSection = document.getElementById('canvas-section') as HTMLDivElement;
  const spanImgStatus = document.getElementById('img-status');
  const spanProcessTime = document.getElementById('processing-time');
  const progressingIcon = document.getElementById('progressing-icon');
  const checkboxCache = document.getElementById('cache') as HTMLInputElement;

  /**
   * Update the Status of processing in the UI
   * @param status
   * @param description
   */
  function updateUIStatus(status?: Status, description?: string) {
    switch (status) {
      case Status.PROCESSING:
        spanImgStatus.innerText = Status.PROCESSING;
        progressingIcon.classList.remove('hidden');
        spanProcessTime.classList.add('hidden');
        break;
      case Status.DONE:
        spanImgStatus.innerText = Status.DONE;
        progressingIcon.classList.add('hidden');
        spanProcessTime.classList.remove('hidden');
        spanProcessTime.innerText = description;
        break;
      default:
        spanImgStatus.innerText = Status.IDLE;
        progressingIcon.classList.add('hidden');
        spanProcessTime.classList.add('hidden');
        break;
    }
  }

  /**
   * Update the image dimension in the UI
   * @param width
   * @param height
   */
  function updateUIDimension(width: number, height: number) {
    const spanImgWidth = document.getElementById('img-width');
    const spanImgHeight = document.getElementById('img-height');
    spanImgWidth.innerText = width.toString();
    spanImgHeight.innerText = height.toString();
  }

  MosaicPhoto.setupUpdateUIFunc(updateUIStatus, updateUIDimension);

  // Setup input file Click event
  const imageFileInput = document.getElementById("image-file") as HTMLInputElement;
  imageFileInput.onchange = () => {
    // Load the original image file
    const imageFile: File = imageFileInput.files[0];
    if (!imageFile) {
      return;
    }

    MosaicPhoto.handleImageFileLoaded(
      canvasSection, imageFile, checkboxCache.checked, TILE_WIDTH, TILE_HEIGHT
    );
  };

  // Setup sample images' Click event
  const sampleImages = document
    .getElementById('sample-images')
    .getElementsByTagName('img');
  for (let i = 0; i < sampleImages.length; i++) {
    (sampleImages[i] as HTMLImageElement).onclick = () => {
      // Clear the current input selected file
      imageFileInput.value = '';
      MosaicPhoto.handleSampleImage(
        canvasSection, sampleImages[i].src, checkboxCache.checked, TILE_WIDTH, TILE_HEIGHT
      );
    }
  }

  // Setup Custom Tile button Click event
  const customTileBtn = document.getElementById("custom-tile-btn") as HTMLButtonElement;
  const inputTileElem = document.getElementById('tileSize') as HTMLInputElement;
  customTileBtn.onclick = () => MosaicPhoto.handleCustomTileSize(
    canvasSection, inputTileElem, checkboxCache.checked
  );
});
