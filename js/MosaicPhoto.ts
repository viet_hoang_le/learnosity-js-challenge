// Module MosaicPhoto holding the logic and functions of converting to mosaic-photo
module MosaicPhoto {
  // Functions for update UI, need to be setup from the outside
  let updateUIStatus: (status?: Status, info?: string) => void;
  let updateUIDimension: (width: number, height: number) => void;

  // Save the timestamp when start DOWNLOAD or PROCESSING a new photo
  let startAt = 0;

  /**
   * Update the status in the UI to be PROCESSING
   * And return the starting timestamp
   */
  function setStatusProcessing(): void {
    updateUIStatus && updateUIStatus(Status.PROCESSING);
    // Reset starting time and request counting number
    startAt = new Date().getTime() / 1000;
  }

  /**
   * Based on param isDone to update the status in the UI to be DONE
   * @param isDone
   * @param requestCount
   */
  function setStatusDone(isDone: boolean, requestCount: number): void {
    if (isDone) {
      // Calculate processing time and the number of requests to display on the UI
      const time = (new Date().getTime() / 1000 - startAt).toFixed(2) + 's';
      updateUIStatus && updateUIStatus(Status.DONE, `${time}, ${requestCount} requests`);
    }
  }

  /**
   * Initialize and setup the Web Worker
   * @param canvas
   * @param gridOfTiles
   * @param cacheColorPng
   * @param tileWidth
   * @param tileHeight
   * @param isUsingCache
   */
  function setupWorker(
    canvas: HTMLCanvasElement,
    gridOfTiles: Array<TileGrid[]>,
    cacheColorPng: CacheColorPng,
    tileWidth: number,
    tileHeight: number,
    isUsingCache: boolean,
  ): Worker {
    console.log('Web Worker Started');
    const worker = new Worker('/js/WebWorker.js');

    const {width, height} = canvas;
    const canvasContext = canvas.getContext('2d');
    canvasContext.clearRect(0, 0, width, height);

    let requestCount = 0;

    const colNumber = Math.ceil(width / tileWidth);

    worker.onmessage = ({data}) => {
      const {cmd, args} = data as WebWorkerData;

      switch (cmd) {
        case WorkerCommand.CALCULATE_AVERAGE_COLOR:
          // When the average-color is ready, processing it
          const {row, col, hexColor} = args as CalculateAverageColorRes;
          // If Cache of Tile is using, try to draw if it is ready
          if (isUsingCache) {
            // Place the holder of Tile in the Grid
            gridOfTiles[row].push({
              column: col,
              hexColor,
            });
            // In case the color in cache is ready (fetched), fulfill all tiles in the grid that has the same hexColor
            if (cacheColorPng[hexColor] && cacheColorPng[hexColor].isReady) {
              gridOfTiles
                .forEach(row => row
                  .filter(tile => tile.hexColor === hexColor)
                  .forEach(tile => tile.tile = cacheColorPng[hexColor].tile));
              // And then try to draw the mosaic
              const isDone = ImageHelper.drawMosaicPhoto(canvasContext, gridOfTiles, colNumber, tileWidth, tileHeight);
              // Update the status if it is Done
              setStatusDone(isDone, requestCount);
              isDone && worker.postMessage({cmd: WorkerCommand.STOP});
            }
          }
          // Checking if a request is needed to send to the server to get color tile
          if (isColorRequestNeeded(cacheColorPng, hexColor, isUsingCache)) {
            // Increase the number of requests to server for tiles
            requestCount++;
            // Ask the worker to fetch the tile
            worker.postMessage({
              cmd: WorkerCommand.FETCH_COLOR,
              args: {...args, tileSize: tileWidth} as FetchColorReq,
            });
          }
          break;

        case WorkerCommand.FETCH_COLOR:
          onTileFetched(
            cacheColorPng,
            gridOfTiles,
            args as FetchColorRes,
            isUsingCache,
          ).then(() => {
            // Try to draw the mosaic photo if all tiles of the row loaded
            // and resolve the (new) current row
            const isDone = ImageHelper.drawMosaicPhoto(canvasContext, gridOfTiles, colNumber, tileWidth, tileHeight);
            setStatusDone(isDone, requestCount);
            isDone && worker.postMessage({cmd: WorkerCommand.STOP});
          });
          break;

        default:
          return;
      }
    };

    return worker;
  }

  /**
   * Initialize the canvas for image processing and
   * @param width
   * @param height
   */
  function initCanvas (
    width: number,
    height: number,
  ): CanvasElements {
    // Initialize the canvas of original Image
    const canvasImage = document.createElement('canvas') as HTMLCanvasElement;
    // Initialize the canvas of mosaic Image
    const canvasMosaic = document.createElement('canvas') as HTMLCanvasElement;
    // Initialize the canvas of mosaic Image custom TileSize
    const canvasMosaicCustom = document.createElement('canvas') as HTMLCanvasElement;

    // Set the canvas dimension
    canvasImage.width = canvasMosaic.width = canvasMosaicCustom.width = width;
    canvasImage.height = canvasMosaic.height = canvasMosaicCustom.height = height;

    return {
      canvasImage,
      canvasMosaic,
      canvasMosaicCustom,
    };
  }

  /**
   * process the Image with different configurations
   * @param canvas
   * @param imageData
   * @param tileWidth
   * @param tileHeight
   * @param isUsingCache
   */
  function processImage(
    canvas: HTMLCanvasElement,
    imageData: ImageData['data'],
    tileWidth: number,
    tileHeight: number,
    isUsingCache: boolean,
  ) {
    // Init a grid of row & column of Tile to draw from the top row to the bottom row
    const gridOfTiles: Array<TileGrid[]> = [];
    // Use a cache of color & image dom to reduce the number of requests sent to the server
    const cacheColorPng: CacheColorPng = {};

    const {width, height} = canvas;

    // Worker for computing average color and fetching tiles
    const webWorker = setupWorker(canvas, gridOfTiles, cacheColorPng, tileWidth, tileHeight, isUsingCache);

    // Calculate the (round up) number of row and column
    const rowNumber = Math.ceil(height / tileHeight);
    const colNumber = Math.ceil(width / tileWidth);

    // Traverse the image row by row
    for (let row = 0; row < rowNumber; row++) {
      const offsetY = row * tileHeight;

      // push the placeholder of row to the grid-of-tiles
      gridOfTiles.push([]);

      // In a row, traverse column by column Tile
      // to calculate the average color and load the according mosaic tile
      for (let col = 0; col < colNumber; col++) {
        const offsetX = col * tileWidth;

        /**
         * Calculate the position of the tile to get its image data
         * The data is get row by row in a tile, which limited by tile height & tile width
         */
        const tileImageData = [];
        for (let i = 0; i < tileHeight; i++) {
          const begin = ((offsetY + i) * width + offsetX) * 4;
          const end = begin + tileWidth * 4;
          tileImageData.push(...Array.prototype.slice.call(imageData.subarray(begin, end)));
        }

        /**
         * Another simpler way is using a dynamic canvas to get the tile image data
         * In term of performance, the both ways are almost same
         */
        /*const canvasBuffer: HTMLCanvasElement = document.createElement('canvas');
        const ctxBuffer = canvasBuffer.getContext('2d');
        // draw the main canvas on our buffer one
        // drawImage(source, source_X, source_Y, source_Width, source_Height,
        //  dest_X, dest_Y, dest_Width, dest_Height)
        ctxBuffer.drawImage(canvasImage, offsetX, offsetY, tileWidth, tileHeight,
          0, 0, tileWidth, tileHeight);
        const tileImageData = ctxBuffer.getImageData(0, 0, tileWidth, tileHeight).data;*/

        // Asking the Worker to calculate the average color of the tile
        webWorker.postMessage({
          cmd: WorkerCommand.CALCULATE_AVERAGE_COLOR,
          args: {row, col, imageData: tileImageData},
        });
      }
    }
  }

  /**
   * Firstly, the hexColor will be checked if an Image is ready for it in cache
   * If yes, adds it to the grid, return FALSE
   * If no, asking a worker to fetch the new tile Image
   * Return TRUE if a Request for Tile of Color is needed
   */
  function isColorRequestNeeded(cacheColorPng: CacheColorPng, hexColor: string, isUsingCache: boolean): boolean {
    if (isUsingCache) {
      // Checking if the color is already in cache
      if (hexColor in cacheColorPng) {
        // The color is already in cache (and is fetching or fetched), don't need to request again
        return false;
      }
      // Cache the color and notify that the color is needed
      cacheColorPng[hexColor] = {
        tile: undefined,
        isReady: false,
      };
    }
    return true;
  }

  /**
   * This function is called when a worker notified that a tile fetched
   * The tile will be store in the gridOfTiles in correct row
   * Cache tileImg of the hexColor to use later
   */
  function onTileFetched(
    cacheColorPng: CacheColorPng,
    gridOfTiles: Array<TileGrid[]>,
    args: FetchColorRes,
    isUsingCache: boolean,
  ): Promise<void> {
    return new Promise<void>((resolve) => {
      const {row, col, hexColor, blob} = args;
      // Create an Image to hold the blob
      const tileImg = new Image;
      // Using a temporary URL to load the blob to the image container
      const url = URL.createObjectURL(blob);
      tileImg.src = url;
      tileImg.onload = function () {
        if (isUsingCache) {
          // Keep the tile image in cache
          cacheColorPng[hexColor] = {
            tile: tileImg,
            isReady: true,
          };
          // Fulfill all tiles in the grid that has the same hexColor
          gridOfTiles
            .forEach(row => row
              .filter(tile => tile.hexColor === hexColor)
              .forEach(tile => tile.tile = tileImg));
        } else {
          gridOfTiles[row].push({
            column: col,
            hexColor,
            tile: tileImg,
          });
        }
        // The browser doesn't need to keep the url reference, simply release it
        URL.revokeObjectURL(url);
        resolve();
      };
    });
  }
  // DOWNLOAD Image from a cross-origin domain
  function startDownload(url: string): Promise<HTMLImageElement> {
    return new Promise<HTMLImageElement> ((resolve) => {
      const downloadedImg = new Image;
      downloadedImg.crossOrigin = "Anonymous";
      downloadedImg.addEventListener("load", () => resolve(downloadedImg), false);
      downloadedImg.src = url;
    });

  }

  // ------------------------- Exported functions

  /**
   * This function is called from outside to pass the update-UI-function for internal usages
   * @param updateUIStatusFunc
   * @param updateUIDimensionFunc
   */
  export function setupUpdateUIFunc(
    updateUIStatusFunc: (status?: Status, info?: string) => void,
    updateUIDimensionFunc: (width: number, height: number) => void
  ) {
    updateUIStatus = updateUIStatusFunc;
    updateUIDimension = updateUIDimensionFunc;
  }

  // Start processing when the Input file changed
  export function handleImageFileLoaded(
    canvasSection: HTMLDivElement,
    imageFile: File,
    isUsingCache: boolean,
    tileWidth: number,
    tileHeight: number,
  ) {
    // Start processing the loaded image
    setStatusProcessing();

    // Loading the image to the canvas and start processing it
    const img = new Image;
    img.src = URL.createObjectURL(imageFile);
    img.onload = function() {
      const {width, height} = img;

      updateUIDimension && updateUIDimension(width, height);

      // Create according canvas for images
      const canvas = initCanvas(width, height);

      // Clear the canvas section
      canvasSection.innerHTML = '';
      // Append the canvas to the dom
      canvasSection.append(canvas.canvasImage, canvas.canvasMosaic, canvas.canvasMosaicCustom);

      // Drawing the original image into the canvas
      const ctxImage = canvas.canvasImage.getContext('2d');
      ctxImage.drawImage(img, 0, 0);
      const imageData = ctxImage.getImageData(0, 0, width, height).data;

      processImage(canvas.canvasMosaic, imageData, tileWidth, tileHeight, isUsingCache);
    };
  }

  // Processing SAMPLE images
  export function handleSampleImage(
    canvasSection: HTMLDivElement,
    src: string,
    isUsingCache: boolean,
    tileWidth: number,
    tileHeight: number,
  ) {
    // Start processing the loaded image
    setStatusProcessing();

    // Load the original image file
    startDownload(src)
      .then((downloadedImg: HTMLImageElement) => {
        const {width, height} = downloadedImg;
        // Loading the image to the canvas and start processing it
        const canvas = initCanvas( width, height);

        // Clear the canvas section
        canvasSection.innerHTML = '';
        // Append the canvas to the dom
        canvasSection.append(canvas.canvasImage, canvas.canvasMosaic, canvas.canvasMosaicCustom);

        const ctxImage = canvas.canvasImage.getContext('2d');
        ctxImage.drawImage(downloadedImg, 0, 0);
        const imageData = ctxImage.getImageData(0, 0, width, height).data;

        processImage(canvas.canvasMosaic, imageData, tileWidth, tileHeight, isUsingCache);

        updateUIDimension && updateUIDimension(width, height);
      });
  }

  // ----------------- CUSTOM TILE SIZE ---------------------- //
  export function handleCustomTileSize(
    canvasSection: HTMLDivElement,
    inputTileElem: HTMLInputElement,
    isUsingCache: boolean,
  ) {
    const TILESIZE_MIN = 4;
    const TILESIZE_MAX = 20;
    try {
      // Parse and get the desire tile size
      let tileSize = inputTileElem.value ? Math.abs(parseInt(inputTileElem.value, 0)) : TILESIZE_MAX;
      if (tileSize < TILESIZE_MIN) {
        tileSize = TILESIZE_MIN;
      }
      inputTileElem.value = tileSize.toString();

      // Checking if there is an Image rendered in the canvas
      if (canvasSection.childNodes.length < 3) {
        return;
      }

      const canvasImage = canvasSection.childNodes[0] as HTMLCanvasElement;
      const canvasMosaicCustom = canvasSection.childNodes[2] as HTMLCanvasElement;

      const {width, height} = canvasImage;

      const ctxImage = canvasImage.getContext('2d');
      const imageData = ctxImage.getImageData(0, 0, width, height).data;

      // Start processing the loaded image
      setStatusProcessing();
      processImage(canvasMosaicCustom, imageData, tileSize, tileSize, isUsingCache);
    } catch (e) {
      console.error('Cannot render Custom TileSize Mosaic', e);
    }
  }
}
