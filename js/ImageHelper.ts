// Convert RGB color to value in Hex system
const rgb2Hex = (r, g, b): string => ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);

// Get the accurate a channel (RGB) value aligning with alpha value
const getRGBvalue = (channel: number, alpha: number) => channel * alpha / 255;

class ImageHelper {
  /**
   * Calculate the average RGB color based on input imageData
   * and then return the hex color value after converted from RGB
   * @param imageData
   */
  static getAverageColor = (imageData: ImageData['data']): string => {
    const rgb: RGB = { r: 0, g: 0, b: 0 };
    try {
      let i = 0;
      let count = 0;
      // Compute total value of R,G,B channels
      while ( (i += 4) < imageData.length ) {
        const alpha = imageData[i+3];
        if (alpha === 0) {
          // In case the pixel is transparent, ignore it
          continue;
        }
        rgb.r += getRGBvalue(imageData[i], alpha);
        rgb.g += getRGBvalue(imageData[i+1], alpha);
        rgb.b += getRGBvalue(imageData[i+2], alpha);
        count++;
      }

      /**
       * Calculate the average value of channels
       * In case count is 0 (transparent point), consider it as white
       * ~~ used to floor average values
       * */

      rgb.r = count > 0 ? ~~(rgb.r / count) : 255;
      rgb.g = count > 0 ? ~~(rgb.g / count) : 255;
      rgb.b = count > 0 ? ~~(rgb.b / count) : 255;

      return rgb2Hex(rgb.r, rgb.g, rgb.b);
    } catch(e) {
      console.error(e);
      return 'ffffff';
    }
  };

  /**
   * Drawing the mosaic photo from currentRow to the bottom
   * Only drawing row which has all tiles loaded
   * Return TRUE if entire of the mosaic-photo drawn
   * @param ctxMosaic
   * @param gridOfTiles the grid of tiles in row & column
   * @param colNumber
   * @param TILE_WIDTH
   * @param TILE_HEIGHT
   */
  static drawMosaicPhoto = (
    ctxMosaic: CanvasRenderingContext2D,
    gridOfTiles: Array<TileGrid[]>,
    colNumber: number,
    TILE_WIDTH: number,
    TILE_HEIGHT: number,
  ): boolean => {
    let currentRow = 0;
    // Get the tiles of the current row (in an array)
    let tilesOfRow = gridOfTiles[currentRow];
    let isTilesOfRowReady = tilesOfRow &&
      tilesOfRow.filter(t => t.tile !== undefined).length === colNumber;

    // offsetX is calculated based on column index of the tile
    const offsetX = (column) => column * TILE_WIDTH;

    // Keep drawing entire row of tiles if it is full loaded
    while (isTilesOfRowReady) {
      // offsetY is not changed for tiles in the same row
      const offsetY = currentRow * TILE_HEIGHT;

      // Using buffer to speed up the drawing of each tile in the row
      const canvasBuffer: HTMLCanvasElement = document.createElement('canvas');
      const ctxBuffer = canvasBuffer.getContext('2d');
      canvasBuffer.width = colNumber * TILE_WIDTH;
      canvasBuffer.height = TILE_HEIGHT;
      tilesOfRow.forEach(({ column, tile }) => {
        ctxBuffer.drawImage(tile, offsetX(column), 0)
      });
      // Push the buffer to a the real DOM canvas
      ctxMosaic.drawImage(canvasBuffer, 0, 0, canvasBuffer.width, TILE_HEIGHT,
        0, offsetY, canvasBuffer.width, TILE_HEIGHT);

      // Processing the next row
      currentRow++;
      tilesOfRow = gridOfTiles[currentRow];
      isTilesOfRowReady = tilesOfRow &&
        tilesOfRow.filter(t => t.tile !== undefined).length === colNumber;
    }

    // Update the new current row which need to be drawn next time
    return currentRow === gridOfTiles.length;
  };
}