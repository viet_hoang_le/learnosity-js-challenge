this.importScripts('./ImageHelper.js');
this.importScripts('./types.js');

/**
 * This Worker is used to
 * calculate average color of a tile based on its color data
 * & load tile image from the server
 */
onmessage = ({data}) => {
  const {cmd, args}: WebWorkerData = data;
  switch (cmd) {
    case WorkerCommand.CALCULATE_AVERAGE_COLOR:
      // Expect to receive tile's offset and its imageData
      const {row, col, imageData} = args as CalculateAverageColorReq;
      // Calculate the hex color value from the data
      const hexColor = ImageHelper.getAverageColor(imageData);
      // And then notify the result
      const resp: WebWorkerData = {
        cmd: WorkerCommand.CALCULATE_AVERAGE_COLOR,
        args: {row, col, hexColor} as CalculateAverageColorRes
      };
      this.postMessage(resp);
      break;

    case WorkerCommand.FETCH_COLOR:
      sendRequest(args as FetchColorReq);
      break;

    case WorkerCommand.STOP:
    default:
      console.log('Web Worker Stopped !');
      close();
  }
};

// Prevent from sending all requests (up to thousands) at the same time (DoS and memory issue)
const requestPool = 50;
let requestCount = 0;
const requests = [];
const loadTileURL = 'http://localhost:8765/color';
/**
 * Sending request to the server
 * @param row
 * @param col
 * @param hexColor
 * @param tileSize
 */
const sendRequest = ({row, col, hexColor, tileSize}) => {
  // Fetching the image data from the server and then convert the buffer to a blob object
  const url = `${loadTileURL}/${hexColor}` + (tileSize ? `/${tileSize}` : '');
  if (requestCount < requestPool) {
    fetch(url)
      .then((response)  => response.blob())
      // Then notify the result after the blob created
      .then((blob) => {
        const resp: WebWorkerData = {
          cmd: WorkerCommand.FETCH_COLOR,
          args: {row, col, hexColor, blob} as FetchColorRes
        };
        this.postMessage(resp);
        requestCount--;
        while (requestCount < requestPool && requests.length > 0) {
          const data = requests.splice(0, 1)[0];
          sendRequest(data);
        }
      });
    requestCount++;
  } else {
    requests.push({row, col, hexColor, tileSize});
  }
};
