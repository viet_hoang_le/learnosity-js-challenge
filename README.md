## Prerequisite
Below steps is for **TypeScript** compilation, not for adding dependent libraries

You don't need to perform below steps if don't want to re-compile TypeScript to JavaScript. 
However, you may need to install `gm` package manually for the server (using `npm add gm`) if do not perform below steps 
1. Install packages `npm ci`
2. Compile to JavaScript `npm run ts:c`
3. Run the server `npm start`

Notice: You also need to install **ImageMagick** into your device.
Reference to [http://imagemagick.org](http://imagemagick.org)


## Guide
- The code is written in **TypeScript**, for stronger code convention & appearance
- The implementation is based on pure JavaScript, no dependent libraries added
- Two Workers are used for different tasks 
  - **WorkerColor.ts** calling average color calculation function
  - **WorkerFetchColor.ts** fetching tiles from the server
- **ImageHelper.ts** provides functions *getAverageColor* and *drawMosaicPhoto*
- **client.ts** implements main logic of the app

## Scope
- The code is test with Chrome Version 71.0.3578.80 (Official Build) (64-bit)
- The browser must support Web Worker
- Because of limitation of time, the UI/UX is not much considered, but only code style, logic, and performance
- **OffscreenCanvas** is a new feature and not fully supported by major browsers, but it is promising to be used in the future [https://developers.google.com/web/updates/2018/08/offscreen-canvas](https://developers.google.com/web/updates/2018/08/offscreen-canvas)
- No optimization for Canvas dimension (width x height) for scaling

Photo mosaic
------------

The goal of this task is to implement the following flow in a client-side app.
1. A user selects a local image file.
2. The app loads that image, divides the image into tiles, computes the average
   color of each tile, fetches a tile from the server for that color, and
   composites the results into a photo mosaic of the original image.
3. The composited photo mosaic should be displayed according to the following
   constraints:
    - tiles should be rendered a complete row at a time (a user should never
      see a row with some completed tiles and some incomplete)
    - the mosaic should be rendered from the top row to the bottom row.
4. The client app should aim to make effective use of parallelism between the
   calculation of colors and the fetching of tiles.

The project skeleton contains a lightweight server (written in node) for
serving the client app and the tile images.  Install the server dependencies
with setup.sh, then run the server with start.sh.  Those scripts assume a Mac
environment, but are trivial enough to replicate on Linux if required.
  /              serves mosaic.html
  /js/*          serves static resources
  /color/<hex>   serves a mosaic tile for color <hex>.  e.g., /color/0e4daa
The tile server generates tile images, and caches them in memory; subsequent
tile requests should be noticably faster.

The tile size should be configurable via the code constants in js/mosaic.js.
The project skeleton is already set up to include those constants in both the
mosaic client and the mosaic server.  The default size is 16x16.

You should:
 - use HTML5 features;
 - write effective comments; pretend you're submitting this for code review;
 - allocate about about 3 hours to do the task, but no more than 4~5 hours.

You may:
 - make minimal use of standard JS libraries (e.g., jQuery), since the goal of
   the exercise is to implement the flow yourself, not call out to
   someLibrary.makePhotoMosaic(...);
 - edit /etc/hosts;
 - use any HTML5 feature supported by current Chrome (e.g., Promise, Worker);
 - be as creative as you like with the submission UI (file input, drag & drop,
   etc); however, it is not the focus of the task, a minimal UI is fine.

Have fun!
