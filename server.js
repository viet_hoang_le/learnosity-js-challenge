// Simulates a mosaic server.
//
// /             serves mosaic.html
// /js/*         servers static files
// /color/<hex>  generates a tile for the color <hex>, and caches it in memory.
//
var mosaic = require('./js/mosaicConfig.js');
var gm = require('gm');
var fs = require('fs');
var http = require('http');
var url = require('url');
var path = require('path');

var dir = path.dirname(fs.realpathSync(__filename));
var mimeTypes = {
    'html': 'text/html',
    'js'  : 'application/javascript'
};
var cache = {};

http.createServer(function (req, res) {
    var pathname = url.parse(req.url).pathname;
    var m;
    if (pathname == '/') {
        res.writeHead(200, {'Content-Type': 'text/html'});
        fs.createReadStream(dir + '/mosaic.html').pipe(res);
        return;
    } else if (m = pathname.match(/^\/js\//)) {
        var filename = dir + pathname;
        var stats = fs.existsSync(filename) && fs.statSync(filename);
        if (stats && stats.isFile()) {
            res.writeHead(200, {'Content-Type' : 'application/javascript'});
            fs.createReadStream(filename).pipe(res);
            return;
        }
    } else if (m = pathname.match(/^\/color\/([0-9a-fA-F]{6})(\/[0-9]+)?/)) {
        var hex = m[1];

        // Custom tile_size, e.g., /color/0e4daa/8 will use size 8 for both width & height of result tile
        let tileSize;
        if (m[2] !== undefined) {
            try {
                tileSize = parseInt(m[2].substring(1), 0);
            }
            catch (e) {
                console.error('Input tile_size is not in correct format');
            }
        }
        const tileWidth = tileSize ? tileSize : mosaic.TILE_WIDTH;
        const tileHeight = tileSize ? tileSize : mosaic.TILE_HEIGHT;
        const eclipseSize = [tileWidth / 2, tileHeight / 2];

        // generate hash for caching
        const hash = `${hex}_${tileWidth}_${tileHeight}`;

        if (hash in cache) {
            complete(cache[hash]);
            return;
        } else {
            var gw = gm(tileWidth, tileHeight, '#ffffff00')
                .fill('#' + hex)
                .stroke('white', 0)
                .drawEllipse(eclipseSize[0] - 0.5, eclipseSize[1] - 0.5, eclipseSize[0] + 0.5, eclipseSize[1] + 0.5)
                .stream('png');
            var chunks = [];
            gw.on('data', function(chunk) { chunks.push(chunk); });
            gw.on('end', function() {
                var buf = Buffer.concat(chunks);
                cache[hash] = buf;
                complete(buf);
            });
            return;
        }
        function complete(buf) {
            res.writeHead(200, {'Content-Type': 'image/png'});
            res.write(buf);
            res.end();
        }
    } else if (pathname.match(/^\/assets\//)){
        var filename = dir + pathname;
        var stats = fs.existsSync(filename) && fs.statSync(filename);
        if (stats && stats.isFile()) {
            res.writeHead(200, {'Content-Type' : 'image/png'});
            fs.createReadStream(filename).pipe(res);
            return;
        }
    }
    res.writeHead(404, {'Content-Type': 'text/plain'});
    res.write('404 Not Found\n');
    res.end();
}).listen(8765, 'localhost');

console.log('mosaic server running on port 8765');
